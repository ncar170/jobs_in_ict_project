var jobRoles = [
    {"role": "Software Developers & Programmers", "jobs": 590, "salaryMax": 100, "salaryMin": 72},

    {"role": "Database & Systems Administrators", "jobs": 74, "salaryMax": 90, "salaryMin": 66},

    {"role": "Help Desk & IT Support", "jobs": 143, "salaryMax": 65, "salaryMin": 46},

    {"role": "Data Analyst", "jobs": 270, "salaryMax": 128, "salaryMin": 69},

    {"role": "Test Analyst", "jobs": 127, "salaryMax": 98, "salaryMin": 70},

    {"role": "Project Management", "jobs": 188, "salaryMax": 190, "salaryMin": 110}
];

var jobDescription = [
    {
        "name": "SoftwareDeveloper", "source": "developer", "title": "Software Developer - Job Description", "skills": [
            "programming languages and techniques", "software development processes such as Agile", "confidentiality, data security and data protection issues."
        ],
        "description": "Software developers and programmers develop and maintain computer software, websites and software applications (apps)."
    },
    {
        "name": "DatabaseAdministrator", "source": "dbadmin", "title": "Database Administrator - Job Description", "skills": [
            "a range of database technologies and operating systems", "new developments in databases and security systems", "computer and database principles and protocols."
        ],
        "description": "Database & systems administrators develop, maintain and administer computer operating systems, database management systems, and security policies and procedures."
    },
    {
        "name": "HelpDesk", "source": "helpdesk", "title": "Help Desk & IT Support - Job Description", "skills": [
            "computer hardware, software, networks and websites", "the latest developments in information technology."
        ],
        "description": "Information technology (IT) helpdesk/support technicians set up computer and other IT equipment and help prevent, identify and fix problems with IT hardware and software."
    },
    {
        "name": "DataAnalyst", "source": "data", "title": "Data Analyst - Job Description", "skills": [
            "data analysis tools such as Excel, SQL, SAP and Oracle, SAS or R", "data analysis, mapping and modelling techniques", "analytical techniques such as data mining."
        ],
        "description": "Data analysts identify and communicate trends in data using statistics and specialised software to help organisations achieve their business aims."
    },
    {
        "name": "TestAnalyst", "source": "test", "title": "Test Analyst - Job Description", "skills": [
            "programming methods and technology", "computer software and systems", "project management."
        ],
        "description": "Test analysts design and carry out testing processes for new and upgraded computer software and systems, analyse the results, and identify and report problems."
    },
    {
        "name": "ProjectManager", "source": "project", "title": "Project Manager - Job Description", "skills": [
            "principles of project management", "approaches and techniques such as Kanban and continuous testing", "how to handle software development issues", "common web technologies used by the scrum team."
        ],
        "description": "Project managers use various methods to keep project teams on track. They also help remove obstacles to progress."
    }
];

/* The makeTable() function creates a table from the data from the jobRoles JSON data and appends it to the div "tableContainer"
* in the HTML document*/

function makeTable() {

    var tableDiv = document.getElementById("tableContainer"); //gets the tableContainer div from the HTML page
    var table = document.createElement("table"); //creates an HTML table element

    table.style.borderStyle = "solid"; //sets the style of the table border to solid

    table.id = "jobsTable";

    var heading = document.createElement("caption");
    heading.innerHTML = "Jobs in ICT - click to view more details"; //creates a caption element and adds text to it

    var thead = document.createElement("thead"); //creates an HTML thead element
    var row = document.createElement("tr"); //creates an HTML tr element
    var column1 = document.createElement("th"); //creates an HTML th element
    column1.innerHTML = "Role"; //places "Role" into the variable column1 (which is a th element)
    var column2 = document.createElement("th");
    column2.innerHTML = "Job Vacancies (08/2018)";
    var column3 = document.createElement("th");
    column3.innerHTML = "Max Salary ($000)";
    var column4 = document.createElement("th");
    column4.innerHTML = "Min Salary ($000)";

    var tbody = document.createElement("tbody"); //creates an HTML tbody element

    row.appendChild(column1); //appends the columns created (as th elements) to the row (as a tr element)
    row.appendChild(column2);
    row.appendChild(column3);
    row.appendChild(column4);

    thead.appendChild(row); //appends the row (tr element) to the thead element

    table.appendChild(heading); //appends the caption to the table itself
    table.appendChild(thead); //appends the thead to the table itself
    table.appendChild(tbody); //appends the tbody to the table itself

    tableDiv.appendChild(table); //adds the table that was created to the tableDiv (tableContainer) in the HTML page

    for (var i = 0; i < jobRoles.length; i++) { //for the length of the jobsRole array, loops through and adds the data
        //from the array to each element
        var row1 = document.createElement("tr");
        row1.id = "Row" + i;
        row1.addEventListener("click", loadSelected, false);//this line adds an event listener (i.e. onclick) to each row, which calls the clicked function that was created
        var cell1 = document.createElement("td");
        cell1.innerHTML = jobRoles[i].role; //places the data within the "role" values of the array into the td elements
        var cell2 = document.createElement("td");
        cell2.innerHTML = jobRoles[i].jobs;
        cell2.style.textAlign = "right";
        var cell3 = document.createElement("td");
        cell3.innerHTML = jobRoles[i].salaryMax;
        cell3.style.textAlign = "right";
        var cell4 = document.createElement("td");
        cell4.innerHTML = jobRoles[i].salaryMin;
        cell4.style.textAlign = "right";

        row1.appendChild(cell1); //appends the cells created (as td elements) to the row (a tr element)
        row1.appendChild(cell2);
        row1.appendChild(cell3);
        row1.appendChild(cell4);

        tbody.appendChild(row1); //appends the row created to the tbody and the table
        table.appendChild(row1);
    }

    total = 0; //set a variable that holds our total
    for (i = 0; i < jobRoles.length; i++) {  //loop through the array
        total += jobRoles[i].jobs;  //add the value in 'jobs' to the total
    }

    averageMax = 0;
    for (i = 0; i < jobRoles.length; i++) {
        averageMax += (jobRoles[i].salaryMax / jobRoles.length);
    }

    averageMin = 0;
    for (i =0; i < jobRoles.length; i++){
        averageMin += (jobRoles[i].salaryMin / jobRoles.length);
    }

    var tfoot = document.createElement("tfoot");
    var footRow = document.createElement("tr");
    var cellf1 = document.createElement("td");
    var cellf2 = document.createElement("td");
    cellf2.innerHTML = "Total: " + total;
    cellf2.style.textAlign = "right";
    var cellf3 = document.createElement("td");
    cellf3.innerHTML = "Average: " + averageMax.toFixed();
    cellf3.style.textAlign = "right";
    var cellf4 = document.createElement("td");
    cellf4.innerHTML = "Average: " + averageMin.toFixed();
    cellf4.style.textAlign = "right";

    footRow.appendChild(cellf1); //appends the summary statistics to the footer of the table
    footRow.appendChild(cellf2);
    footRow.appendChild(cellf3);
    footRow.appendChild(cellf4);
    tfoot.appendChild(footRow);
    table.appendChild(footRow);

}

/* The loadRole() function loads a role that will be placed into the HTML document on the loading of the page (i.e body onload = loadRole())
 into the relevant divs.*/

function loadRole(role) {

    var imageContainer = document.getElementById("imageContainer");
    var textContainer = document.getElementById("textContainer");

    var roleImage = document.createElement("img");//creates img element and assigns the featured image to it
    roleImage.src = "../Images/" + jobDescription[role].source + ".jpg";
    roleImage.alt = jobDescription[role].name;
    roleImage.title = jobDescription[role].name;

    roleImage.id = "featuredImage";

    var roleTitle = document.createElement("h4");
    roleTitle.innerHTML = jobDescription[role].title;

    roleTitle.id = "featuredTitle";

    var roleDesc = document.createElement("p");
    roleDesc.innerHTML = jobDescription[role].description;

    roleDesc.id = "featuredDesc";

    var roleSkills = document.createElement("h5");
    roleSkills.innerHTML = "Skills and Knowledge";

    roleSkills.id = "featuredSkills";

    /* createList creates a list from the "skills" section of the jobDescription JSON array and adds each skill to the list*/

    const createList = jobDescription => {
        const roleList = document.createElement("ul"); //creates an unordered list
        roleList.id = "featuredList"; //assigns the created list an ID, which is needed later on in the changeRole() function
        jobDescription.forEach((e) => {
            const listItem = document.createElement("li"); //creates list elements for the length of the loop
            listItem.innerHTML = e;
            roleList.appendChild(listItem);
        });
        return roleList;
    };

    const skillsList = createList(jobDescription[role].skills); //this passes the "skills" array into the createList function and
                                                                //assigns it to skillsList, which is then appended to containerDiv
    imageContainer.appendChild(roleImage);
    textContainer.appendChild(roleTitle);
    textContainer.appendChild(roleDesc);
    textContainer.appendChild(roleSkills);
    textContainer.appendChild(skillsList);
}

// This creates a random number (between 0 and the length of the JSON array), which is then passed into the loadRole method (which
// loads the randomly assigned role on the loading of the page).

function loadRandom() {
    var random = Math.floor(Math.random() * (jobDescription.length));
    loadRole(random);
}

//This function changes the details of the role that was loaded on to the page at random depending on what line of the
// table is clicked

function changeRole(role) {

    // var imageContainer = document.getElementById("imageContainer");
    var textContainer = document.getElementById("textContainer");

    var roleImage = document.getElementById("featuredImage");
    roleImage.src = "../Images/" + jobDescription[role].source + ".jpg";
    roleImage.alt = jobDescription[role].name;
    roleImage.title = jobDescription[role].name;

    var roleTitle = document.getElementById("featuredTitle");
    roleTitle.innerHTML = jobDescription[role].title;

    var roleDesc = document.getElementById("featuredDesc");
    roleDesc.innerHTML = jobDescription[role].description;

    var roleSkills = document.getElementById("featuredSkills");
    roleSkills.innerHTML = "Skills and Knowledge";

    //To change the list, get the id of the list, remove it from its parent div, and then create a new list
    var fList = document.getElementById("featuredList");
    textContainer.removeChild(fList);

    const createList = jobDescription => {
        const roleList = document.createElement("ul");
        roleList.id = "featuredList";
        jobDescription.forEach((e) => {
            const listItem = document.createElement("li"); //creates list elements for the length of the loop
            listItem.innerHTML = e;
            roleList.appendChild(listItem);
        });
        return roleList;
    };

    const skillsListChange = createList(jobDescription[role].skills);
    textContainer.appendChild(skillsListChange);
}

//This function gets the row index of the row that is clicked (see row1.addEventListener in makeTable() function
//The row that is clicked is then passed into the changeRole() function to change the role on the page

function loadSelected() {

    //     alert("clicked tr: " + event.target.parentNode.id);

    row = this.rowIndex - 1; //this gets the rowIndex from the event (i.e. rowID), and subtract 1 to get the correct position
    changeRole(row);//the variable row is passed into the changeRole function which changes the role in the HTML document

}

